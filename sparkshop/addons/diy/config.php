<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai  <876337011@qq.com>
// +----------------------------------------------------------------------

return [
    'skip_auth' => [
        'addons/diy/admin.api/getGoodsCate' => 1,
        'addons/diy/admin.api/getGoodsList' => 1,
        'addons/diy/admin.api/getSlider' => 1,
        'addons/diy/admin.api/getArticleCate' => 1,
        'addons/diy/admin.api/getArticle' => 1,
    ]
];